﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessBlog.Services
{
    public interface IAuthenticationService
    {

        void SignIn(string Id ,string login, bool createPersistentCookie);

        void SignOut();

        //User GetAuthenticatedUser();

        string EncriptToMD5(string text);
    }
}
