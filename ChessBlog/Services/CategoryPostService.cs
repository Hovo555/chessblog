﻿using ChessBlog.EntityCore.Repositories;
using ChessBlog.EntityData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace ChessBlog.Services
{
    public class CategoryPostService : ICategoryPostService
    {
        private readonly IGenericRepository<CategoryPost> _repo;
        private readonly HttpContextBase _httpContext;

        public CategoryPostService(HttpContextBase httpContext, IGenericRepository<CategoryPost> repo)
        {

            _httpContext = httpContext;
            _repo = repo;
        }

        public IList<CategoryPost> GetCategory()
        {
            int userId = Convert.ToInt32(_httpContext.User.Identity.Name);
            var category = (from e in _repo.Table where e.User.Id == userId && e.ParrentId == 0 select e).ToList();
            return category;

        }

        public void function()
        {


            int userId = Convert.ToInt32(_httpContext.User.Identity.Name);      
            CategoryPost cat = new CategoryPost
            {
                UserId = userId,
                Date = DateTime.Now,
                Name = "FirstPOO",
                ParrentId = 3,
            };

            _repo.Insert(cat);

        }
    }
}