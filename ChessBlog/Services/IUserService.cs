﻿using ChessBlog.Areas.Admin.Models;
using ChessBlog.EntityData;
using System.Collections.Generic;

namespace ChessBlog.Services
{
    public interface IUserService
    {
        User GetUserLoginPass(string login, string password);
        IList<CategoryPost> GetCategoryPostById(int id);
        IList<CategoryPost> GetPost(int pagesize);
        IList<CategoryPost> GetPostPaging(int pagesize, int page, int CatId, string date, string tag);
        IList<CategoryPost> PostSearched(string tag, int pagesize, int page);
        int PostSearchedPaging(string tag, int PageSize);
        int GetPostCount(int PageSize, int CatId, string date, string tag);
        IList<CategoryPost> GetCategorywithPagging(int PageSize, int Page);
        IList<CategoryPost> GetCategory();
        IList<CategoryPost> GetAllCategory();
        int Insert(PhotoVideoLinkView model);
        int InsertCategory(string Name);
        void UpdateCategory(int Id, string Name);
        void DeleteCategory(int Id, string Name);
        void SavePost(int id);

    }
}
