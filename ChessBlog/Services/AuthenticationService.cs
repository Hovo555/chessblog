﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Security;

namespace ChessBlog.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly HttpContextBase _httpContext;
        public AuthenticationService(HttpContextBase httpContext)
        {
            _httpContext = httpContext;
        }

        public void SignIn(string Id, string userName, bool createPersistentCookie)
        {
            var now = DateTime.UtcNow.ToLocalTime();

            var ticket = new FormsAuthenticationTicket(
                1 /*version*/,
                Id,
                now,
                now.AddHours(10),
                createPersistentCookie,
                userName,
                FormsAuthentication.FormsCookiePath);

            var encryptedTicket = FormsAuthentication.Encrypt(ticket);

            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            cookie.HttpOnly = true;
            if (ticket.IsPersistent)
            {
                cookie.Expires = ticket.Expiration;
            }
            else
            {
                cookie.Expires = DateTime.MinValue;
            }
            cookie.Secure = FormsAuthentication.RequireSSL;
            cookie.Path = FormsAuthentication.FormsCookiePath;
            if (FormsAuthentication.CookieDomain != null)
            {
                cookie.Domain = FormsAuthentication.CookieDomain;
            }

            _httpContext.Response.Cookies.Add(cookie);
        }

        public void SignOut()
        {

            FormsAuthentication.SignOut();
        }

        public string EncriptToMD5(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            byte[] data = md5.ComputeHash(Encoding.ASCII.GetBytes(text));

            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }
    }
}