﻿using ChessBlog.Areas.Admin.Models;
using ChessBlog.EntityCore.Repositories;
using ChessBlog.EntityData;
using ChessBlog.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;


namespace ChessBlog.Services
{
    public class UserService : IUserService
    {
        private readonly Logger _log;
        private readonly IGenericRepository<User> _repo;
        private readonly IGenericRepository<CategoryPost> _repoCategoryPost;
        private readonly IGenericRepository<Tag> _repoTags;
        private readonly HttpContextBase _httpContext;

        public UserService(IGenericRepository<User> repo, HttpContextBase httpContext, IGenericRepository<CategoryPost> repoCategoryPost, IGenericRepository<Tag> tags)
        {
            _log = Logger.log;
            _repo = repo;
            _httpContext = httpContext;
            _repoCategoryPost = repoCategoryPost;
            _repoTags = tags;
        }

        public User GetUserLoginPass(string login, string password)
        {
            if (string.IsNullOrWhiteSpace(login))
                return null;


            var user = (from e in _repo.Table where e.Login == login && e.Password == password select e).FirstOrDefault();



            //var result = _repo.ExecuteStoredProcedure<User>(
            //    "GetUserByLoginPassword",
            //    new SqlParameter { ParameterName = "Login", Value = login != null ? (object)login : DBNull.Value, SqlDbType = SqlDbType.NVarChar },
            //    new SqlParameter { ParameterName = "Password", Value = password != null ? (object)password : DBNull.Value, SqlDbType = SqlDbType.NVarChar }
            //    );

            return user;
        }

        public IList<CategoryPost> GetCategory()
        {
            int userId = Convert.ToInt32(_httpContext.User.Identity.Name);
            var category = (from e in _repoCategoryPost.Table where e.User.Id == userId && e.ParrentId == 0 && e.Deleted == 0 select e).ToList();
            return category;

        }
        public IList<CategoryPost> GetCategorywithPagging(int PageSize, int page)
        {
            int userId = Convert.ToInt32(_httpContext.User.Identity.Name);
            var category = (from e in _repoCategoryPost.Table
                            where
                            e.User.Id == userId && e.ParrentId == 0 && e.Deleted == 0
                            orderby e.Id descending
                            select e).Skip(PageSize * (page - 1)).Take(PageSize).ToList();
            return category;




            //return (from cust in _repoCategoryPost.Table
            //        where cust.ParrentId != 0 && cust.Deleted == 0
            //        orderby cust.Id descending
            //        select cust).Skip(PageSize * (page - 1)).Take(PageSize).ToList();

        }
        public IList<CategoryPost> GetAllCategory()
        {

            var category = (from e in _repoCategoryPost.Table where e.ParrentId == 0 && e.Deleted == 0 select e).ToList();
            return category;

        }

        public int Insert(PhotoVideoLinkView model)
        {
            List<PhotoVideoLink> PhotoVideo = model?.PhotoVideoList?.Select(item => new PhotoVideoLink
            {
                PhotoNameExt = item.PostedFileBase == null ? "" : Path.GetFileNameWithoutExtension(item.PostedFileBase.FileName) + Path.GetExtension(item.PostedFileBase.FileName),
                Text = item.Text ?? "",
                VideoLink = item.VideoLink ?? ""
            }).ToList();


            int userId = Convert.ToInt32(_httpContext.User.Identity.Name);
            CategoryPost ob = new CategoryPost
            {
                UserId = userId,
                Date = DateTime.Now,
                Name = model.PostText.Trim(),
                CountVisit = 0,
                Deleted = 1,
                ParrentId = model.CategoryId,
                PhotoVideoLink = PhotoVideo,
            };

            _repoCategoryPost.Insert(ob);


            foreach (var item in model.Tags)
            {
                Tag tags = new Tag
                {
                    TagName = item,
                    CategoryPostId = ob.Id
                };
                _repoTags.Insert(tags);
            }
            return ob.Id;
        }



        public int InsertCategory(string Name)
        {

            int userId = Convert.ToInt32(_httpContext.User.Identity.Name);
            CategoryPost ob = new CategoryPost
            {
                UserId = userId,
                Date = DateTime.Now,
                Name = Name,
                CountVisit = 0,
                Deleted = 0,
                ParrentId = 0
            };

            _repoCategoryPost.Insert(ob);

            return ob.Id;
        }

        public void UpdateCategory(int Id, string Name)
        {
            int userId = Convert.ToInt32(_httpContext.User.Identity.Name);
            _repoCategoryPost.Update(new CategoryPost { Id = Id, Name = Name, UserId = userId });
        }

        public void DeleteCategory(int Id, string Name)
        {
            int userId = Convert.ToInt32(_httpContext.User.Identity.Name);
            _repoCategoryPost.Update(new CategoryPost { Id = Id, UserId = userId, Name = Name, Deleted = 1 });
        }


        public IList<CategoryPost> GetCategoryPostById(int id)
        {
            var category = (from e in _repoCategoryPost.Table where e.Id == id select e).ToList();
            return category;
        }

        public void SavePost(int id)
        {
            CategoryPost c = (from x in _repoCategoryPost.Table where x.Id == id select x).First();
            c.Deleted = 0;
            _repoCategoryPost.Update(c);
        }

        public IList<CategoryPost> GetPost(int pagesize)
        {


            return (from cust in _repoCategoryPost.Table
                    where cust.ParrentId != 0 && cust.Deleted == 0
                    orderby cust.Id descending
                    select cust).Take(pagesize).ToList();

        }







        public int GetPostCount(int PageSize, int CatId, string date, string tag)
        {



            var postsQuery = from p in _repoCategoryPost.Table select p;
            postsQuery = postsQuery.Where(p => p.Deleted == 0);

            if (CatId > 0)
            {
                postsQuery = postsQuery.Where(p => p.ParrentId == CatId);
            }
            else
            {
                postsQuery = postsQuery.Where(p => p.ParrentId != 0);
            }
            if (date != "")
            {
                DateTime startdate = DateTime.Parse(date);
                DateTime enddate = startdate.AddDays(1);
                postsQuery = postsQuery.Where(p => p.Date >= startdate && p.Date < enddate);
            }
            if (tag != "")
            {
                var ListpostId = (from p in _repoTags.Table where p.TagName == tag orderby p.CategoryPostId descending select p.CategoryPostId).ToList();
                postsQuery = postsQuery.Where(p => ListpostId.Contains(p.Id)).OrderByDescending(p => p.Id);
            }




            int count = Convert.ToInt32(Math.Ceiling((double)postsQuery.Count() / PageSize));
            return count;








            //if (CatId > 0)
            //{

            //    if (date != "")
            //    {
            //        DateTime startdate = DateTime.Parse(date);
            //        DateTime enddate = startdate.AddDays(1);
            //        int count = Convert.ToInt32(Math.Ceiling((double)_repoCategoryPost.Table.Where(x => x.ParrentId == CatId && x.Deleted == 0 && x.Date >= startdate && x.Date < enddate).Count() / PageSize));
            //        return count;



            //    }
            //    else
            //    {

            //        int count = Convert.ToInt32(Math.Ceiling((double)_repoCategoryPost.Table.Where(x => x.ParrentId == CatId && x.Deleted == 0).Count() / PageSize));
            //        return count;
            //    }
            //}
            //else
            //{
            //    if (date != "")
            //    {
            //        DateTime startdate = DateTime.Parse(date);
            //        DateTime enddate = startdate.AddDays(1);
            //        int count = Convert.ToInt32(Math.Ceiling((double)_repoCategoryPost.Table.Where(x => x.ParrentId != 0 && x.Deleted == 0 && x.Date >= startdate && x.Date < enddate).Count() / PageSize));
            //        return count;
            //    }
            //    else
            //    {
            //        int count = Convert.ToInt32(Math.Ceiling((double)_repoCategoryPost.Table.Where(x => x.ParrentId != 0 && x.Deleted == 0).Count() / PageSize));
            //        return count;
            //    }
            //}

        }

        public IList<CategoryPost> GetPostPaging(int PageSize, int page, int CatId, string date, string tag)
        {


            var postsQuery = from p in _repoCategoryPost.Table select p;
            postsQuery = postsQuery.Where(p => p.Deleted == 0);
            if (CatId > 0)
            {
                postsQuery = postsQuery.Where(p => p.ParrentId == CatId);
            }
            else
            {
                postsQuery = postsQuery.Where(p => p.ParrentId != 0);
            }
            if (date != "")
            {
                DateTime startdate = DateTime.Parse(date);
                DateTime enddate = startdate.AddDays(1);
                postsQuery = postsQuery.Where(p => p.Date >= startdate && p.Date < enddate);
            }
            if (tag != "")
            {
                var ListpostId = (from p in _repoTags.Table where p.TagName == tag orderby p.CategoryPostId descending select p.CategoryPostId).ToList();
                postsQuery = postsQuery.Where(p => ListpostId.Contains(p.Id)).OrderByDescending(p => p.Id);
            }

            postsQuery = postsQuery.OrderByDescending(p => p.Id).Skip(PageSize * (page - 1)).Take(PageSize);

            return postsQuery.ToList();


            // _log.LogMethod("mtav metod" + PageSize + ":" + page + ":" + CatId + ":" + date);


            //if (CatId > 0)
            //{

            //    if (date != "")
            //    {
            //        DateTime startdate = DateTime.Parse(date);
            //        DateTime enddate = startdate.AddDays(1);
            //        return (from cust in _repoCategoryPost.Table
            //                where cust.ParrentId == CatId && cust.Deleted == 0 && cust.Date >= startdate && cust.Date < enddate
            //                orderby cust.Id descending
            //                select cust).Skip(PageSize * (page - 1)).Take(PageSize).ToList();
            //    }
            //    else
            //    {

            //        return (from cust in _repoCategoryPost.Table
            //                where cust.ParrentId == CatId && cust.Deleted == 0
            //                orderby cust.Id descending
            //                select cust).Skip(PageSize * (page - 1)).Take(PageSize).ToList();
            //    }

            //}

            //else
            //{
            //    if (date != "")
            //    {
            //        DateTime startdate = DateTime.Parse(date);
            //        DateTime enddate = startdate.AddDays(1);

            //        //  _log.LogMethod("start end" + startdate + ":" + enddate);

            //        return (from cust in _repoCategoryPost.Table
            //                where cust.ParrentId != 0 && cust.Deleted == 0 && cust.Date >= startdate && cust.Date < enddate
            //                orderby cust.Id descending
            //                select cust).Skip(PageSize * (page - 1)).Take(PageSize).ToList();
            //    }
            //    else
            //    {
            //        return (from cust in _repoCategoryPost.Table
            //                where cust.ParrentId != 0 && cust.Deleted == 0
            //                orderby cust.Id descending
            //                select cust).Skip(PageSize * (page - 1)).Take(PageSize).ToList();
            //    }

            //}

        }

        public IList<CategoryPost> PostSearched(string tag, int PageSize, int page)
        {
            var postId = (from p in _repoTags.Table
                          where p.TagName == tag
                          select p.CategoryPostId).ToList();


            var postsQuery = from p in _repoCategoryPost.Table select p;
            postsQuery = postsQuery.Where(p => postId.Contains(p.Id)).OrderByDescending(p => p.Id);
            postsQuery = postsQuery.OrderByDescending(p => p.Id).Skip(PageSize * (page - 1)).Take(PageSize);

            return postsQuery.ToList();

        }

        public int PostSearchedPaging(string tag, int PageSize)
        {
            var postId = (from p in _repoTags.Table
                          where p.TagName == tag
                          select p.CategoryPostId).ToList();



            int count = Convert.ToInt32(Math.Ceiling((double)_repoTags.Table.Count() / PageSize));
            return count;
        }



        //public IList<CategoryPost> GetSingleCategoryPost(int CategoryId, int pagesize)
        //{


        //    return (from cust in _repoCategoryPost.Table
        //            where cust.Deleted == 0 && cust.ParrentId == CategoryId
        //            orderby cust.Id descending
        //            select cust).Take(pagesize).ToList();

        //}

        //public int GetSingleCategoryPostCount(int CategoryId, int PageSize)
        //{
        //    int count = Convert.ToInt32(Math.Ceiling((double)_repoCategoryPost.Table.Where(x => x.ParrentId == CategoryId && x.Deleted == 0).Count() / PageSize));
        //    return count;
        //}

    }

}
