﻿using System;
using System.IO;

namespace ChessBlog.Extensions
{
    public class Logger
    {
        private static Logger _instance = null;

        public static Logger log => _instance ?? (_instance = new Logger());

        private Logger()
        {

        }

        public void LogMethod(string exeption)
        {

            string path = AppDomain.CurrentDomain.BaseDirectory;
            //C:\Users\Hovo\Desktop\NotifySenderApi\NotifySenderApi\


            string filename = "logs-" + DateTime.Now.ToString("dd-MM-yyyy");

            var file = path + "logs\\" + filename + ".txt";

            //  var file = System.IO.Path.Combine("logs", filename + ".txt");

            if (!System.IO.File.Exists(file))
            {
                System.IO.File.WriteAllText(file, exeption);
            }
            else
            {
                using (var w = File.AppendText(file))
                {
                    string timeStamp = DateTime.Now.ToString("dd/MM/yyyy-HH.mm.ss.ffffff");

                    w.WriteLine();
                    w.WriteLine("-------------------------------" + timeStamp + "-----------------------------------");
                    w.Write(exeption);
                }

            }
        }
    }
}