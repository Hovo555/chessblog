﻿using System;

namespace ChessBlog.Extensions
{
    public static class RelativTime
    {
        public static string RelativDate(this DateTime dt)
        {

            var ts = new TimeSpan(DateTime.Now.Ticks - dt.Ticks);
            double delta = Math.Abs(ts.TotalSeconds);

            if (delta < 60)
            {
                return ts.Seconds == 1 ? "մեկ վայրկյան առաջ" : ts.Seconds + " վայրկյան առաջ";
            }
            if (delta < 120)
            {
                return "րոպե առաջ";
            }
            if (delta < 2700) // 45 * 60
            {
                return ts.Minutes + " րոպե առաջ";
            }
            if (delta < 5400) // 90 * 60
            {
                return "ժամ առաջ";
            }
            if (delta < 86400) // 24 * 60 * 60
            {
                return ts.Hours + " ժամ առաջ";
            }
            if (delta < 172800) // 48 * 60 * 60
            {
                return "օր առաջ";
            }
            if (delta < 604800) // 7 * 24 * 60 * 60
            {
                return ts.Days + " օր առաջ";
            }
            return dt.ToShortDateString();
        }
    }
}