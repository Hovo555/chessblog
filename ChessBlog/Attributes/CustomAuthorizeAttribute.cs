﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ChessBlog.Attributes
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        private IList<string> allowedUsers = new List<string>();
        private IList<string> allowedRoles = new List<string>();

        protected override bool AuthorizeCore(HttpContextBase httpContext)

        {
            if (!String.IsNullOrEmpty(base.Users))
                allowedUsers = SplitString(base.Users);

            if (!String.IsNullOrEmpty(base.Roles))
                allowedRoles = SplitString(base.Roles);


            return httpContext != null && httpContext.Request != null && httpContext.Request.IsAuthenticated && (httpContext.User.Identity is FormsIdentity)
                 && CheckUser(httpContext) && CheckRole(httpContext);

        }

        private bool CheckUser(HttpContextBase httpContext)
        {
            if (allowedUsers.Count > 0)
            {

                return allowedUsers.Contains(httpContext.User.Identity.Name);
            }
            return true;
        }

        private bool CheckRole(HttpContextBase httpContext)
        {
            if (allowedRoles.Count > 0)
            {
                //var _userService = DependencyResolver.Current.GetService<IUserService>();
                //var userRoles = _userService.GetUserRoles(httpContext.User.Identity.Name);

                //var userRolesNames = userRoles.Select(x => x.Name);

                // if (!allowedRoles.Intersect(userRolesNames).Any())
                //    return false;
            }
            return true;
        }


        IList<string> SplitString(string original)
        {
            if (String.IsNullOrEmpty(original))
            {
                return new List<string>();
            }

            var split = from piece in original.Split(',')
                        let trimmed = piece.Trim()
                        where !String.IsNullOrEmpty(trimmed)
                        select trimmed;
            return split.ToList();
        }
    }
}