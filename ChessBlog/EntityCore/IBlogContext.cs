﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace ChessBlog.EntityCore
{
    public interface IBlogContext
    {
        IDbSet<TEntity> Set<TEntity>() where TEntity : class;
        int SaveChanges();
        Database Database { get; }
        DbEntityEntry Entry(object entity);
    }
}
