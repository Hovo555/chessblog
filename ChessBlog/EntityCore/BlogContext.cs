﻿using ChessBlog.EntityMap;
using System.Data.Entity;

namespace ChessBlog.EntityCore
{
    public class BlogContext : DbContext, IBlogContext
    {
        public BlogContext() : base("PostConnection") { }

        //DbSet<User> User { get; set; }
        //DbSet<CategoryPost> Post { get; set; }
        //DbSet<PhotoVideoLink> PhotoVideoLink { get; set; }    

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new CategoryPostMap());
            modelBuilder.Configurations.Add(new PhotoVideoLinkMap());
        }

    }
}