﻿using ChessBlog.EntityData;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace ChessBlog.Areas.Admin.Models
{
    public class PhotoVideoLinkView
    {
        public string PostText { get; set; }
        public ICollection<PhotoVideo> PhotoVideoList { get; set; }

        public ICollection<string> Tags { get; set; }
        //---------------------------for dropdown----------------------//
        public int CategoryId { get; set; }
        public ICollection<CategoryPost> CategoryList { get; set; }
    }

    public class PhotoVideo
    {

        [Display(Name = "Photo")]
        public HttpPostedFileBase PostedFileBase { get; set; }
        [Display(Name = "VideoLink")]
        public string VideoLink { get; set; }
        [Display(Name = "Text")]
        public string Text { get; set; }

        //[Required(ErrorMessage = "պարտադիր է")]
    }
}