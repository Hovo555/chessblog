﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ChessBlog.Areas.Admin.Models
{
    //public class AddEditCategory
    //{
    //    [Required(ErrorMessage = "պարտադիր է")]
    //    public string Name { get; set; }
    //    public List<CategoryPostView> CategoryList { get; set; } = new List<CategoryPostView>();
    //}

    public class CategoryPostView
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }
        [Required(ErrorMessage = "պարտադիր է")]
        public string Name { get; set; }
        public DateTime Date { get; set; }
    }
}