﻿using ChessBlog.Areas.Admin.Models;
using ChessBlog.Attributes;
using ChessBlog.EntityData;
using ChessBlog.Services;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace ChessBlog.Areas.Admin.Controllers
{

    [HandleError]
    [CustomAuthorize]
    public class HomeController : Controller
    {
        private readonly IUserService _repo;


        public HomeController(IUserService repo)
        {
            _repo = repo;
        }


        // GET: Admin/Home

        public ActionResult Index()
        {

            var model = new PhotoVideoLinkView { CategoryList = _repo.GetCategory() };
            return View(model);
        }

        [ChildActionOnly]
        public ActionResult AdminHeaderLinks()
        {
            ViewBag.IsAdmin = false;
            if (Request.IsAuthenticated && (User.Identity is FormsIdentity))
            {
                // if(_repoService.IsAdmin(User.Identity.Name))
                ViewBag.IsAdmin = true;

            }
            return PartialView();
        }


        [HttpPost, ValidateInput(false)]
        public ActionResult PostPreview(PhotoVideoLinkView model)
        {
            int id = 0;
            if (ModelState.IsValid && model.PostText != null)
            {
                id = _repo.Insert(model);

                if (model?.PhotoVideoList != null)
                {
                    foreach (var item in model.PhotoVideoList)
                    {
                        if (item.PostedFileBase != null)
                        {
                            string strMappath = "~/UploadedImages/" + id + "/";
                            if (!Directory.Exists(Server.MapPath(strMappath)))
                            {
                                DirectoryInfo di = Directory.CreateDirectory(Server.MapPath(strMappath));
                            }
                            var fileName = Path.GetFileNameWithoutExtension(item.PostedFileBase.FileName) +
                                           Path.GetExtension(item.PostedFileBase.FileName);
                            var destinationPath = Path.Combine(Server.MapPath(strMappath), fileName);
                            item.PostedFileBase.SaveAs(destinationPath);
                        }
                    }
                }
            }
            else
            {
                ModelState.AddModelError("PostText", "Post Text Empty or Model not valid");
                model.CategoryList = _repo.GetCategory();
                return View("Index", model);
            }


            var current = _repo.GetCategoryPostById(id);


            Session.Abandon();
            return View(current.FirstOrDefault());
        }

        [HttpPost]
        public ActionResult PostSave(int Id, string name)
        {
            if (name != null && name == "ok")
            {
                _repo.SavePost(Id);
                return View("Index", new PhotoVideoLinkView { CategoryList = _repo.GetCategory() });
            }
            var model = new PhotoVideoLinkView { CategoryList = _repo.GetCategory() };
            return View("Index", model);

        }

        [HttpGet]
        public ActionResult Categories()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Category_Get([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                IList<CategoryPost> DateEF;
                if (request.PageSize == 0)
                {
                    DateEF = _repo.GetCategory();
                }
                else
                {
                    DateEF = _repo.GetCategorywithPagging(request.PageSize, request.Page);
                }

                // Getting all Customer data    


                var CategoryPostView = new List<CategoryPostView>();
                foreach (var item in DateEF)
                {
                    CategoryPostView.Add(new CategoryPostView { Id = item.Id, Name = item.Name, Date = item.Date });
                }

                var result = new DataSourceResult()
                {
                    Data = CategoryPostView,
                    Total = _repo.GetCategory().Count()
                };

                return Json(result);
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        [HttpPost]
        public JsonResult Category_Create([DataSourceRequest] DataSourceRequest request, CategoryPostView model)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    model.Id = _repo.InsertCategory(model.Name);
                }
                return Json(new[] { model }.ToDataSourceResult(request, ModelState));

            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public ActionResult Category_Update([DataSourceRequest] DataSourceRequest request, CategoryPostView model)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    _repo.UpdateCategory(model.Id, model.Name);
                }
                return Json(new[] { model }.ToDataSourceResult(request, ModelState));

            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public ActionResult Category_Delete([DataSourceRequest] DataSourceRequest request, CategoryPostView model)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    _repo.DeleteCategory(model.Id, model.Name);
                }
                return Json(new[] { model }.ToDataSourceResult(request, ModelState));

            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public ActionResult PostEditDelete()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Container()
        {
            if (Session["n"] != null)
            {
                Session["n"] = Convert.ToInt32(Session["n"]) + 1;
                ViewBag.n = Convert.ToInt32(Session["n"]);
            }
            else
            {
                Session["n"] = 0;
                ViewBag.n = 0;
            }
            return PartialView();
        }

        public ActionResult Container_delete()
        {
            int n = 0;
            if (Session["n"] != null)
            {
                n = Convert.ToInt32(Session["n"]);
                Session["n"] = Convert.ToInt32(Session["n"]) - 1;
            }

            return Json(n, JsonRequestBehavior.AllowGet);
        }
    }
}