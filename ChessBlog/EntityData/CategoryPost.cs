﻿using System;
using System.Collections.Generic;

namespace ChessBlog.EntityData
{
    public class CategoryPost
    {
        public int Id { get; set; }

        public int ParrentId { get; set; }

        public string Name { get; set; }

        public DateTime Date { get; set; }

        public int Deleted { get; set; }

        public int CountVisit { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }

        public virtual ICollection<PhotoVideoLink> PhotoVideoLink { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }

        //public CategoryPost()
        //{
        //        foreach (var VARIABLE in this.Tags)
        //        {
        //            new BlogContext().Configuration.LazyLoadingEnabled = false;
        //        }
        //}
        //}


    }
}