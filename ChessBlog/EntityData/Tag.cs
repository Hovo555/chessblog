﻿namespace ChessBlog.EntityData
{
    public class Tag
    {
        public int Id { get; set; }

        public string TagName { get; set; }

        public int CategoryPostId { get; set; }

        public CategoryPost CategoryPost { get; set; }
    }
}