﻿namespace ChessBlog.EntityData
{
    public class PhotoVideoLink
    {
        public int Id { get; set; }

        public string PhotoNameExt { get; set; }

        public string VideoLink { get; set; }
       
        public string Text { get; set; }

        public CategoryPost CategoryPost { get; set; }
    }
}