﻿using ChessBlog.EntityData;
using System;
using System.Collections.Generic;

namespace ChessBlog.Models
{
    public class PostViewModel
    {
        public ICollection<CategoryPost> CategoryList { get; set; }
        public IEnumerable<CategoryPost> Data { get; set; }
        public int NumberOfPages { get; set; }
        public int CurrentPage { get; set; }
        public DateTime DateTime { get; set; }
        public int SelectedCategoryId { get; set; }
        public int Scroll { get; set; }
        public string SelectedDate { get; set; }
        public string SelectedSearch { get; set; }
    }
}