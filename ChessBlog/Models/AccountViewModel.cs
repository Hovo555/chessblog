﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ChessBlog.Models
{
    public class AccountViewModel
    {
        [Required(ErrorMessage = "Լրացնել դաշտը")]
        [Display(Name = "Մուտքանուն")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Լրացնել դաշտը")]
        [DataType(DataType.Password)]
        [Display(Name = "Գաղտնաբառ")]
        public string Password { get; set; }

        [Display(Name = "Հիշե՞լ")]
        public bool RememberMe { get; set; }     
    }
}