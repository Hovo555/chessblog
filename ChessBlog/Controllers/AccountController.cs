﻿using ChessBlog.EntityCore.Repositories;
using ChessBlog.EntityData;
using ChessBlog.Models;
using ChessBlog.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChessBlog.Controllers
{

    [Authorize]
    public class AccountController : Controller
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly IUserService _userService;
        public AccountController(IAuthenticationService authenticationService, IUserService userService)
        {
            _authenticationService = authenticationService;
            _userService = userService;
        }
        // GET: Account//
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home", new { area = "Admin" });

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult Login(AccountViewModel model, string returnUrl)
        {
            model.Login = model.Login.Trim();
            var user = _userService.GetUserLoginPass(model.Login, _authenticationService.EncriptToMD5(model.Password));
            if (user != null)
            {
                _authenticationService.SignIn(user.Id.ToString(), user.Login, model.RememberMe);
                if (Url.IsLocalUrl(returnUrl))
                    return RedirectToAction("Index", "Home", new { area = "Admin" });
                else
                    return RedirectToAction("Index", "Home", new { area = "Admin" });
            }
            else
            {
                ModelState.AddModelError("LoginPasswordError", "Սխալ մուտքանուն կամ գաղտնաբառ");
            }
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            _authenticationService.SignOut();
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }
    }
}