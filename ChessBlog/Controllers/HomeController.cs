﻿using ChessBlog.Extensions;
using ChessBlog.Models;
using ChessBlog.Services;
using System;
using System.Web.Mvc;

namespace ChessBlog.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserService _repo;
        private readonly Logger _log;

        public HomeController(IUserService repo)
        {
            _log = Logger.log;
            _repo = repo;
        }
        public const int PageSize = 5;
        //public ActionResult Index()
        //{

        //    var post = new PostViewModel();
        //    post.Data = _repo.GetPost(PageSize);
        //    post.CategoryList = _repo.GetAllCategory();
        //    post.NumberOfPages = _repo.GetPostCount(PageSize, 0, "");
        //    post.CurrentPage = 1;
        //    return View(post);
        //}

        //public ActionResult _PostContent(int page, int CatId, string date)
        //{
        //    var post = new PostViewModel();
        //    post.Data = _repo.GetPostPaging(PageSize, page, CatId, date);
        //    post.NumberOfPages = _repo.GetPostCount(PageSize, CatId, date);

        //    return PartialView(post);
        //}

        public ActionResult About()
        {
            return View();
        }
        public ActionResult Contact()
        {
            return View();
        }
        public ActionResult PostContent(int page = 1, string date = "", int catid = 0, string tag = "")
        {
            try
            {
                var post = new PostViewModel();
                post.CategoryList = _repo.GetAllCategory();
                post.Data = _repo.GetPostPaging(PageSize, page, catid, date, tag);
                post.NumberOfPages = _repo.GetPostCount(PageSize, catid, date, tag);
                post.CurrentPage = page;
                post.SelectedCategoryId = catid;
                post.SelectedDate = date;
                // post.SelectedSearch = tag;
                return View(post);
            }
            catch (Exception ex)
            {
                _log.LogMethod("PostContent" + ex);
                return View();
            }

        }
        [HttpGet]
        public ActionResult Post(int Id)
        {
            try
            {
                var post = new PostViewModel();
                post.CategoryList = _repo.GetAllCategory();
                post.Data = _repo.GetCategoryPostById(Id);
                return View(post);
            }
            catch (Exception ex)
            {
                _log.LogMethod("Post" + ex);
                return View();
            }
        }
    }
}