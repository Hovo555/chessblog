namespace ChessBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class rrr : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Tags", name: "CategoryPost_Id1", newName: "CategoryPostId");
            RenameIndex(table: "dbo.Tags", name: "IX_CategoryPost_Id1", newName: "IX_CategoryPostId");
            DropColumn("dbo.Tags", "CategoryPost_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tags", "CategoryPost_Id", c => c.Int(nullable: false));
            RenameIndex(table: "dbo.Tags", name: "IX_CategoryPostId", newName: "IX_CategoryPost_Id1");
            RenameColumn(table: "dbo.Tags", name: "CategoryPostId", newName: "CategoryPost_Id1");
        }
    }
}
