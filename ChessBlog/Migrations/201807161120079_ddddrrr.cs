namespace ChessBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ddddrrr : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Login = c.String(nullable: false, maxLength: 50),
                        Password = c.String(nullable: false, maxLength: 50),
                        Name = c.String(nullable: false, maxLength: 50),
                        Surname = c.String(nullable: false, maxLength: 50),
                        Email = c.String(nullable: false, maxLength: 50),
                        Roll = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CategoryPost",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParrentId = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        Date = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Deleted = c.Int(nullable: false),
                        CountVisit = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.PhotoVideoLink",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PhotoNameExt = c.String(nullable: false),
                        VideoLink = c.String(nullable: false),
                        Text = c.String(nullable: false),
                        CategoryPost_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CategoryPost", t => t.CategoryPost_Id, cascadeDelete: true)
                .Index(t => t.CategoryPost_Id);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TagName = c.String(),
                        CategoryPost_Id = c.Int(nullable: false),
                        CategoryPost_Id1 = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CategoryPost", t => t.CategoryPost_Id1, cascadeDelete: true)
                .Index(t => t.CategoryPost_Id1);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CategoryPost", "UserId", "dbo.User");
            DropForeignKey("dbo.Tags", "CategoryPost_Id1", "dbo.CategoryPost");
            DropForeignKey("dbo.PhotoVideoLink", "CategoryPost_Id", "dbo.CategoryPost");
            DropIndex("dbo.Tags", new[] { "CategoryPost_Id1" });
            DropIndex("dbo.PhotoVideoLink", new[] { "CategoryPost_Id" });
            DropIndex("dbo.CategoryPost", new[] { "UserId" });
            DropTable("dbo.Tags");
            DropTable("dbo.PhotoVideoLink");
            DropTable("dbo.CategoryPost");
            DropTable("dbo.User");
        }
    }
}
