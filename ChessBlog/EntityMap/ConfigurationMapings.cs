﻿using ChessBlog.EntityData;
using System.Data.Entity.ModelConfiguration;

namespace ChessBlog.EntityMap
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            this.ToTable("User");
            this.HasKey<int>(s => s.Id);
            this.Property(p => p.Login).HasMaxLength(50).IsRequired();
            this.Property(p => p.Password).HasMaxLength(50).IsRequired();
            this.Property(p => p.Name).HasMaxLength(50).IsRequired();
            this.Property(p => p.Surname).HasMaxLength(50).IsRequired();
            this.Property(p => p.Email).HasMaxLength(50).IsRequired();
            this.Property(p => p.Roll).HasMaxLength(50).IsRequired();
            this.HasMany(p => p.CategoryPost).WithRequired(p => p.User).HasForeignKey(x => x.UserId);
        }
    }
    public class CategoryPostMap : EntityTypeConfiguration<CategoryPost>
    {
        public CategoryPostMap()
        {
            this.ToTable("CategoryPost");
            this.HasKey<int>(s => s.Id);
            this.Property(p => p.ParrentId).IsRequired();
            this.Property(p => p.Name).HasColumnType("nvarchar(max)").IsRequired();       
            this.Property(p => p.Date).HasColumnType("datetime2").IsRequired();
            this.HasMany(p => p.PhotoVideoLink).WithRequired(p => p.CategoryPost);
            this.HasMany(p => p.Tags).WithRequired(p => p.CategoryPost);
        }
    }

    public class PhotoVideoLinkMap : EntityTypeConfiguration<PhotoVideoLink>
    {

        public PhotoVideoLinkMap()
        {
            this.ToTable("PhotoVideoLink");
            this.HasKey<int>(s => s.Id);        
            this.Property(p => p.Text).HasColumnType("nvarchar(max)").IsRequired();
            this.Property(p => p.PhotoNameExt).HasColumnType("nvarchar(max)").IsRequired();
            this.Property(p => p.VideoLink).HasColumnType("nvarchar(max)").IsRequired();
        }
    }

}