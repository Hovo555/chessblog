﻿using System.Web.Optimization;

namespace ChessBlog
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-ui-1.12.1.js",
                        "~/Scripts/datepicker-hy.js"));


            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));



            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));


            bundles.Add(new ScriptBundle("~/bundles/bootstrapdatetimepicker").Include(
                "~/Scripts/bootstrap-datetimepicker.js"));

            bundles.Add(new ScriptBundle("~/Scripts/tinymce/tinymce").Include(
                    "~/Scripts/tinymce/tinymce.min.js",
                    "~/Scripts/tinymce/jquery.tinymce.min.js",
                    "~/Scripts/tinymce/plugins/autoresize/plugin.min.js",
                    "~/Scripts/tinymce/plugins/charmap/plugin.min.js",
                    "~/Scripts/tinymce/plugins/code/plugin.min.js",
                    "~/Scripts/tinymce/plugins/image/plugin.min.js",
                    "~/Scripts/tinymce/plugins/table/plugin.min.js",
                    "~/Scripts/tinymce/plugins/textcolor/plugin.min.js",
                    "~/Scripts/tinymce/themes/modern/theme.min.js"));
            //------------------------------------------style----------------------------------------------

            bundles.Add(new StyleBundle("~/bundles/Content/css").Include(
                     "~/Content/bootstrap.css",
                     "~/Content/site.css"));

            //bundles.Add(new StyleBundle("~/Content/css/font-awesome").Include(
            //    "~/Content/css/fontawesome-all.css", new CssRewriteUrlTransform())); // save real path

            bundles.Add(new StyleBundle("~/Content/css/font-awesome").Include(
                    "~/Content/css/fontawesome-all.css"));




        }
    }
}
